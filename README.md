# mkfontrom

Process a bitmap font into a format that can be read as an initializer for Verilog block memory using `$readmemb`.

## how to use

Running `mkfontrom` as shown below will read a font from `myfont.png`, assuming it contains a grid of characters that are 5 pixels wide and 8 pixels tall. It will write Verilog-compatible `$readmemb` output into a file called `myfont.png.mem` in the current working directory.

```
mkfontrom myfont.png 5 8
```

The optional `--output` argument can be used to specify a different output file. For example, to write to a file called `my_verilog/output.txt`:

```
mkfontrom myfont.png 5 8 --output my_verilog/output.txt
```

## input format

Input files should consist of a grid of characters, where any non-black pixels will be interpreted as part of a character. Note that each pixel is saved as a single bit -- either "on" or "off".

Characters should be arranged in row-first order without extra padding between.

The ['klostro' font repository](https://gitlab.com/antonok/klostro) contains a sample of a correctly-formatted input file for `mkfontrom`.

### example

In the following example, the resulting buffer would contain letters in alphabetical order. The image file is 8 characters wide by 4 characters tall.

```
myfont.png (ascii mockup)
----------------------------

   ABCDEFGH
   IJKLMNOP
   QRSTUVWX
   YZ123456

```

Assuming each character is 10 pixels wide and 12 pixels tall, the image should have a resolution of 80x48.

The following command would then be used to generate a memory initializer buffer in the file `my_rom.mem`.

```
   mkfontrom input.png 10 12 --output my_rom.mem
```

## output format

A single line of output will be created for each character in the input image. Each line will contain either a 0 or a 1 for each pixel of its corresponding character.

### example

Suppose the following 4x6 character is in the top-left position of a font bitmap.

```
ascii mockup     pixel color (1 = white, 0 = black)
------------     ----------------------------------
   █  █                       1001
   ██ █                       1101
   █ ██                       1011
   █  █                       1001
   █  █                       1001
   █  █                       1001
```

The first line of the output file from `mkfontrom` would then be as shown below.

```
100111011011100110011001
```

Note that the result is simply the "pixel color" column from above, but with newlines removed.

## verilog usage

The following sample code, modified lightly from Quartus II's `Single Port ROM` template, will infer a block ROM module initialized with the contents of mkfontrom_output.txt.

```verilog
module font_rom
#(parameter DATA_WIDTH=8, parameter ADDR_WIDTH=8)
(
	input [(ADDR_WIDTH-1):0] addr,
	input clk, 
	output reg [(DATA_WIDTH-1):0] q
);

	reg [DATA_WIDTH-1:0] rom[2**ADDR_WIDTH-1:0];

	initial
	begin
		$readmemb("mkfontrom_output.txt", rom);
	end

	always @ (posedge clk)
	begin
		q <= rom[addr];
	end

endmodule
```
